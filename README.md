# Sun Sensor PCB Design

The purpose of the pcb is to tranfer of the received signal from the sun sensor to the microcontroller. For better signal quality we use a filter and an amplifier. On the board there are two different layouts for the signal processing.

## First Layout

The signal goes through the filter, then goes to amplifier and finally to the output pins.

## Second Layout

The signal "breaks in two", the one goes to the filter and the second one goes to the amplifier, then after they come together again and go to the output pins.
